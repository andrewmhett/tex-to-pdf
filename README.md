# tex-to-pdf
## Requirements
- `latexmk`
## Usage
The `tex-to-pdf` script is essentially a wrapper for the `latexmk` command which deletes all the unused output files produced by `latexmk`. This script passes all of its flags and arguments to `latexmk`, however it does specify the `-pdf` flag on its own.
